sealed trait IO[A] // the IO language
case class Const[A](a: A) extends IO[A]
case object ReadLn extends IO[String]
case class WriteLn(s: String) extends IO[Unit]
case class Compose[A, B](a: IO[A], b: A => IO[B]) extends IO[B]

object IO {
  import scalaz.syntax.monad._
  type AbstractSyntaxDecoratorProxy[A[_]] = scalaz.Monad[A]

  implicit object IOSyntax extends AbstractSyntaxDecoratorProxy[IO] {
    def point[A](a: => A): IO[A] = Const(a)
    def bind[A, B](ioa: IO[A])(f: A => IO[B]) =
      Compose(ioa, f)
  }

  def compose[A, B](io: IO[A])(f: A => IO[B]) =
    Compose(io, f)

  def runPure[A](p: IO[A], in: List[String], out: List[String] = Nil): (A, List[String], List[String]) =
    p match {
      case Const(a)   => (a, in, out)
      case ReadLn     => (in.head, in.tail, out)
      case WriteLn(x) => ((), in, x :: out)
      case Compose(ioa, f) =>
        val (a, in2, out2) = runPure(ioa, in, out)
        runPure(f(a), in2, out2)
    }

  // An interpreter for IO programs
  def run[A](p: IO[A]): A = p match { // pattern matching
    case Const(a)   => a
    case ReadLn     => Console.readLine()
    case WriteLn(x) => Console.println(x)
    case Compose(ioa, f) => // IO[Any], Any => IO[A] 
      val any = run(ioa) // Any
      val nxt = f(any) // IO[A]
      run(nxt) // A
  }

  val echo = compose(ReadLn)(WriteLn)

  val helloWorld = compose(Const("Hello World!"))(WriteLn)

  val helloJohn =
    compose(WriteLn("What is your name?")) {
      _ =>
        compose(ReadLn) {
          s => WriteLn("hello " + s)
        }
    }

  val helloJim =
    for {
      _ <- WriteLn("What is your name?")
      s <- ReadLn
      _ <- WriteLn("hello " + s)
    } yield ()
}