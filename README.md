Guerilla's Guide to Functional Programming

contans a Keynote presentation and the PDF output.

To run the code, you need to have sbt installed, then simply:

    sbt

to get the sbt console up. Then to bring up a Scala console:

    > console

Once on the Scala console you can:

    scala> import IO._
    import IO._

    scala> runPure(helloWorld, List())
    res0: (Unit, List[String], List[String]) = ((),List(),List(Hello World!))